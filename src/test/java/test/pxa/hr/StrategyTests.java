package test.pxa.hr;

import com.pxa.hr.client.HRConsoleApp;
import com.pxa.hr.client.model.OutputHierarchyElement;
import com.pxa.hr.client.strategies.EmployeeHierarchyStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HRConsoleApp.class)
public class StrategyTests {
    @Autowired
    private EmployeeHierarchyStrategy strategy;

    // todo: these tests are dumb because they assume the base dataset for the exercise.

    @Test
    public void pullAllFromOneDown() {
        Map<String, OutputHierarchyElement> outputMap = strategy.loadEmployeeHierarchyFrom(1);

        Assert.assertNotNull(outputMap);

        Assert.assertTrue(outputMap.values().size()==1);

        outputMap.values().stream().forEach(item -> {
            Assert.assertTrue(item.getId()==1);
            Assert.assertTrue(item.getReports().size()==3); // should only have 3 reports
        });

        System.out.println(outputMap.keySet().stream().collect(Collectors.joining()));
    }

    @Test
    public void pullAllFromTwoDown() {
        Map<String, OutputHierarchyElement> outputMap = strategy.loadEmployeeHierarchyFrom(2);

        Assert.assertNotNull(outputMap);

        Assert.assertTrue(outputMap.values().size()==1);

        outputMap.values().stream().forEach(item -> {
            Assert.assertTrue(item.getId()==2);
            Assert.assertTrue(item.getReports().size()==1); // should only have 1 report
        });

    }

    @Test
    public void pullAllFromHarryDown() {
        Map<String, OutputHierarchyElement> outputMap = strategy.loadEmployeeHierarchyFrom(100);

        Assert.assertNotNull(outputMap);

        Assert.assertTrue(outputMap.values().size()==1);

        outputMap.values().stream().forEach(item -> {
            Assert.assertTrue(item.getId()==100);
            Assert.assertTrue(item.getReports().size()==0); // should only have 1 report
        });

    }
}
