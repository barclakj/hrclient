package com.pxa.hr.client.rest;

public class HttpClientError extends Error {
    public HttpClientError(Throwable cause) {
        super(cause);
    }
}
