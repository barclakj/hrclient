package com.pxa.hr.client.rest;


import com.pxa.hr.client.model.EmployeeViewAPIResponse;

/**
 * Simple client interface.
 */
public interface HRRestClient {
    /**
     * Fetches an employee by id from HR system.
     * If the employee does not exist then NULL is returned.
     * Other errors are thrown as either Client or Server issues.
     * @param employeeId
     * @return
     * @throws HttpClientError
     */
    EmployeeViewAPIResponse getEmployee(final int employeeId) throws HttpClientError;
}
