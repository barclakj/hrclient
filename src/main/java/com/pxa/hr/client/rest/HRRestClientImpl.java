package com.pxa.hr.client.rest;

import com.pxa.hr.client.model.EmployeeViewAPIResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@Slf4j
@ConfigurationProperties(prefix="client", ignoreUnknownFields = false)
@Component
/**
 * Basic client implementation
 */
public class HRRestClientImpl implements HRRestClient {
    private final RestTemplate restTemplate;

    private String employeeEndpoint;

    public HRRestClientImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void setEmployeeEndpoint(String employeeEndpoint) {
        this.employeeEndpoint = employeeEndpoint;
    }

    @Override
    public EmployeeViewAPIResponse getEmployee(final int employeeId) throws HttpClientError {
        log.debug("Calling {} ", employeeEndpoint + employeeId);
        EmployeeViewAPIResponse employee = null;
        try {
            employee = restTemplate.getForObject(employeeEndpoint + employeeId, EmployeeViewAPIResponse.class);
        } catch (HttpClientErrorException e) {
            log.info("Request for employee {} returned {}", employeeId, e.getRawStatusCode());
            if (e.getRawStatusCode()!=404) {
                log.warn("Non 404 error occurred! May be client or server related {}.", e.getMessage());
                throw new HttpClientError(e);
            }
        }
        return employee;
    }


}

