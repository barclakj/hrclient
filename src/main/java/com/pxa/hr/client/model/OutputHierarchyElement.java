package com.pxa.hr.client.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
/**
 * Represents specified output format to be jsonified.
 */
public class OutputHierarchyElement {
    private Map<String, OutputHierarchyElement> reports;
    private int id;
}
