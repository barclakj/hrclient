package com.pxa.hr.client.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
/**
 * Ideally this should reflect the API response and be shared w/ server or generated
 * from API spec. Duplicated here for ease.
 */
public class EmployeeViewAPIResponse {
    private int id;
    private String name;
    private String title;
    private List<Integer> reports;
}
