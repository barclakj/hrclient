package com.pxa.hr.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.pxa.hr.client.strategies.EmployeeHierarchyStrategy;
import com.pxa.hr.client.strategies.IterativeEmployeeHierarchyStrategy;
import com.pxa.hr.client.strategies.RecursiveEmployeeHierarchyStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@Slf4j
@SpringBootApplication
public class HRConsoleApp implements CommandLineRunner {
    @Autowired
    public EmployeeHierarchyStrategy strategy;

    public static void main(String[] args) {
        SpringApplication.run(HRConsoleApp.class, args);
    }

    @Bean
    public EmployeeHierarchyStrategy getStrategy() {
        return new RecursiveEmployeeHierarchyStrategy();
        // return new IterativeEmployeeHierarchyStrategy();
    }

    @Override
    public void run(String... args) throws IOException {
        int EMPLOYEE_ID = 1; // change this to pull back another employee as root of the hierarchy..

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(System.out, strategy.loadEmployeeHierarchyFrom(EMPLOYEE_ID));
        System.out.flush();
    }
}
