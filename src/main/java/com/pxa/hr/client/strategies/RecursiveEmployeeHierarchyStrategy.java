package com.pxa.hr.client.strategies;

import com.pxa.hr.client.model.EmployeeViewAPIResponse;
import com.pxa.hr.client.model.OutputHierarchyElement;
import com.pxa.hr.client.rest.HRRestClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementation of a hierarchy strategy using recursion.
 *
 * Recursive is simplest model and deemed sufficient given constraint appears
 * to be a max of 2000 employees..
 */
public class RecursiveEmployeeHierarchyStrategy  implements EmployeeHierarchyStrategy {
    @Autowired
    private HRRestClient client;

    @Override
    public Map<String, OutputHierarchyElement> loadEmployeeHierarchyFrom(int employeeId) {
        return drillDownHierarchy(employeeId);
    }

    /**
     * This method recursively drills down the hierarchy pulling back client
     * details on each report identified.
     *
     *
     * @param employeeId
     * @return
     */
    public Map<String, OutputHierarchyElement> drillDownHierarchy(final int employeeId) {
        // get employee
        EmployeeViewAPIResponse employeeResult = client.getEmployee(employeeId);

        // create an output element formatted as per spec and copy
        // api result to the specified form
        OutputHierarchyElement parentHierarchyElement = new OutputHierarchyElement();
        parentHierarchyElement.setReports(new LinkedHashMap<String, OutputHierarchyElement>());
        parentHierarchyElement.setId(employeeId);

        // for each child
        employeeResult.getReports().stream()
                .forEach(reportId -> {
                    // recursively get the reports details which comes back as
                    // a map of 1 elements.
                    Map<String, OutputHierarchyElement> reportMap = drillDownHierarchy(reportId);
                    assert(reportMap.keySet().size()==1);
                    for(String name : reportMap.keySet()) {
                        parentHierarchyElement.getReports().put(name, reportMap.get(name));
                    }
                });

        // the output format is a map of names so return a mapped pair for the current element
        Map<String, OutputHierarchyElement> parentMap = new LinkedHashMap<>();
        parentMap.put(employeeResult.getName(), parentHierarchyElement);

        return parentMap;
    }
}
