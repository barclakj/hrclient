package com.pxa.hr.client.strategies;

import com.pxa.hr.client.model.OutputHierarchyElement;

import java.util.Map;

/**
 * Interface allowing for multiple hierarchy
 *  exploration strategies to be implemented.
 */
public interface EmployeeHierarchyStrategy {
    /**
     * Performs a deep search of the employee hierarchy from the specified
     * employee id down. Returns a map structured as per expected output json.
     * @param employeeId
     * @return
     */
    Map<String, OutputHierarchyElement> loadEmployeeHierarchyFrom(final int employeeId);
}
