package com.pxa.hr.client.strategies;

import com.pxa.hr.client.model.EmployeeViewAPIResponse;
import com.pxa.hr.client.model.OutputHierarchyElement;
import com.pxa.hr.client.rest.HRRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of a hierarchy strategy using iteration instead of recursion.
 *
 * This is a more complex method and heavy on memory usage (could be improved)
 * but not preferable due to complexity when the recursive model should be sufficient.
 */
@Slf4j
public class IterativeEmployeeHierarchyStrategy implements EmployeeHierarchyStrategy {
    @Autowired
    private HRRestClient client;

    @Override
    public Map<String, OutputHierarchyElement> loadEmployeeHierarchyFrom(final int employeeId) {
        // hashmap to store all employee details in.
        Map<Integer, EmployeeViewAPIResponse> allEmployees = new HashMap<>();

        // load all employees from the specified id down
        log.debug("Pulling back employees from: {}", employeeId );
        loadAllEmployees(allEmployees, employeeId);

        // transform all employees from the relatively flat structure returned
        // by the API into hierarchy with the new output hierarchy format
        log.debug("Converting employees to output hierarchy elements.");
        Map<Integer, OutputHierarchyElement> allEmployeesTransformed = convertAllEmployeeResult(allEmployees);

        // root is the element from the specified id.
        log.debug("Fudging root from indexed to named element.");
        EmployeeViewAPIResponse root = allEmployees.get(employeeId);
        OutputHierarchyElement rootHierarchyElement = allEmployeesTransformed.get(employeeId); // I'm assuming here  1 = root!
        Map<String, OutputHierarchyElement> rootMap = new LinkedHashMap<>();
        rootMap.put(root.getName(),rootHierarchyElement);
        return rootMap;
    }

    /**
     * loads all employees
     * @param allEmployees
     */
    private void loadAllEmployees(final Map<Integer, EmployeeViewAPIResponse> allEmployees, final int employeeId) {
        List<Integer> missingEmployees = new ArrayList<>();
        missingEmployees.add(employeeId); // we know ("only know") about employee 1!

        // avoiding recursion...
        // we fetch each employee we know we're missing
        // identify any reports which we don't already know about for each employee fetched
        // flatten the sum of all missing reportees
        // repeat until there's no missing employees.
        do {
            log.debug("Iterating over another {} employees...", missingEmployees.size());
            missingEmployees = missingEmployees.stream()
                    .map((id) -> {
                        return loadEmployeeAndIdentifyNewReports(allEmployees, id);
                    })
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
        } while(missingEmployees.size()>0);
    }

    /**
     * Load an employee and identify any new reports we're missing.
     * @param id
     * @return
     */
    private List<Integer> loadEmployeeAndIdentifyNewReports(final Map<Integer, EmployeeViewAPIResponse> allEmployees, final int id) {
        log.debug("loadEmployeeAndIdentifyNewReports for employee {}", id);

        List<Integer> newlyIdentifiedEmployees = new ArrayList<>();
        EmployeeViewAPIResponse employeeResult = client.getEmployee(id);
        if (employeeResult!=null) {
            allEmployees.put(employeeResult.getId(), employeeResult);
            employeeResult.getReports().stream()
                    .forEach((reportingEmployeeId) -> {
                        if (!allEmployees.containsKey(reportingEmployeeId)) {
                            newlyIdentifiedEmployees.add(reportingEmployeeId);
                        }
                    });
        }
        log.debug("Found {} newly identified employees", newlyIdentifiedEmployees.size());
        return newlyIdentifiedEmployees;
    }


    /**
     * Converts the employee hierarchy into the desired output format as a map.
     * @param allEmployees
     * @return
     */
    private Map<Integer, OutputHierarchyElement> convertAllEmployeeResult(final Map<Integer, EmployeeViewAPIResponse> allEmployees) {
        log.debug("convertAllEmployeeResult for {} employee results", allEmployees.size());
        Map<Integer, OutputHierarchyElement> allConvertedEmployeeResults = new LinkedHashMap<>();

        allEmployees.values().stream()
                .forEach(employeeResult -> {
                    OutputHierarchyElement converted = convertEmployeeResult(employeeResult);
                    allConvertedEmployeeResults.put(employeeResult.getId(), converted);
                });

        allEmployees.values().stream()
                .forEach(employeeResult -> {
                    OutputHierarchyElement leaderConverted = allConvertedEmployeeResults.get(employeeResult.getId());
                    Map<String, OutputHierarchyElement> leaderReports = leaderConverted.getReports();

                    employeeResult.getReports().stream()
                            .forEach( reporteeId -> {
                                EmployeeViewAPIResponse reportee = allEmployees.get(reporteeId);
                                OutputHierarchyElement reporteeConverted = allConvertedEmployeeResults.get(reporteeId);
                                leaderReports.put(reportee.getName(), reporteeConverted);
                            });
                });
        return allConvertedEmployeeResults;
    }

    /**
     * Converts an employee from the API format to one as per output spec.
     * @param employeeResult
     * @return
     */
    private OutputHierarchyElement convertEmployeeResult(final EmployeeViewAPIResponse employeeResult) {
        OutputHierarchyElement t = new OutputHierarchyElement();
        t.setId(employeeResult.getId());
        t.setReports(new LinkedHashMap<String, OutputHierarchyElement>());
        return t;
    }
}
