# HR Client

If the server is running locally then build should be accomplished via a simple.. 

mvn clean package

However, if the server isn't running then 
integration tests will fail so build via

mvn clean package -DskipTests

Application can then run via:

java -jar hrclient-0.1-SNAPSHOT.jar


